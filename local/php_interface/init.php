<?php
use \Bitrix\Main\Loader;
$classes = [
    'Parser' => '/local/libs/Parser/Parser.php',
];
Loader::registerAutoLoadClasses(null, $classes);
Loader::autoLoad('Parser');