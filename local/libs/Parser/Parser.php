<?php

declare(strick_type = 1);

namespace local\libs\parser;

class Parser
{
    private $symbols = ['BTC', 'ETH', 'XRP'];
    private $pricePerRub;
    private $pricePerDollar;

    public function __construct() {
        $path = $_SERVER['DOCUMENT_ROOT'] . '/env.json';
//        if (!file_exists($path)) {
//            throw new Exception("Не найден файл: $path");

//        }
        $url = json_decode(file_get_contents($path), true);
        $ch = curl_init($url["url"]);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $head = curl_exec($ch);
        curl_close($ch);
        $data = json_decode($head);
        foreach ($data as $key => $value) {
            $objVars = get_object_vars($value);
            $symbols = array_values($this->symbols);
            $this->checkValues($symbols, $objVars);
        }
    }

    public function getPricePerDollar()
    {
        return $this->pricePerDollar;
    }
    public function getPricePerRub()
    {
        return $this->pricePerRub;
    }
    private function setPricePerRub(string $pricePerRub, string $symbol): void
    {
        $this->pricePerRub[$symbol] = $pricePerRub;
    }

    private function setPricePerDollar(string $pricePerDollar, string $symbol): void
    {
        $this->pricePerDollar[$symbol] = $pricePerDollar;
    }
    private function checkValues(array $symbols, array $array) : void {
        foreach ($symbols as $symbol) {
            if (in_array($symbol, $array)) {
                $this->setPricePerDollar($array['price_usd'], $symbol);
                $this->setPricePerRub($array['price_rub'], $symbol);

            }
        }
    }

}