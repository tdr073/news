<?php

namespace local\libs;

use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

$classes = [
    'Parser' => '/local/libs/Parser/Parser.php',
];

try {
    Loader::registerAutoLoadClasses(null, $classes);
} catch (LoaderException $e) {
}
Loader::autoLoad('Parser');